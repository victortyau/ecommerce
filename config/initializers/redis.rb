# frozen_string_literal: true

class Redis
  def self.redis
    @redis ||= Redis.new(host: 'redis_cache', port: 6378, db: 1, password: ENV.fetch('REDIS_CACHE_PASSWD'))
  end
end
