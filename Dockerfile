ARG BASE_RUBY_IMAGE_TAG='3.1.3-alpine'

FROM ruby:${BASE_RUBY_IMAGE_TAG} as base

ARG RAILS_ENV

ENV RAILS_ENV=${RAILS_ENV}

# m1 support
ARG TARGETARCH
RUN if [ "${TARGETARCH}" = "arm64" ]; then \
  apk add --no-cache \
  # needed for nokogiri on arm64
  gcompat; \
  fi;

RUN apk add --no-cache \
    build-base \
    # Required for container healthcheck
    curl \
    # Required for blunder
    git \
    # Required for typhoeus gem
    libcurl \
    # Required for connecting to postgres
    postgresql-dev \
    # Required for pg_dump when using rails migration locally
    postgresql-client \
    tzdata \
    # Needed for mimemagic gem
    shared-mime-info

WORKDIR /app

COPY Gemfile Gemfile.lock ./

RUN gem install bundler -v 2.3.15

RUN MAKE="make --jobs 5" bundle install --jobs=5 --no-cache --retry=5 && \
    bundle config unset rubygems.pkg.github.com

COPY . ./

# Needed at runtime. Set here so earlier layers can be cached
ARG GIT_SHA
ENV GIT_SHA=${GIT_SHA:-development}
ENV DD_VERSION=${GIT_SHA}

# This script will run anytime this docker image starts
# This allows us the run the db migrations automatically.
ENTRYPOINT ["/app/bin/entrypoint"]

CMD ["bundle", "exec", "rails", "server", "-p", "3000"]
