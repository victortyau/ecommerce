# frozen_string_literal: true

class CategorySerializer < ApplicationSerializer
  attr_reader :category

  def initialize(category)
    @category = category
  end

  def serialize
    {
      id: category.id,
      name: category.name
    }
  end
end
