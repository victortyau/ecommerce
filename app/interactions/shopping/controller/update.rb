# frozen_string_literal: true

module Shopping
  module Controller
    class Update < ApplicationInteraction
      object :shopping
      hash :shopping_params do
        string :transaction
      end

      def execute
        shopping.update(shopping_params) ? serialize(shopping, with: ShoppingSerializer) : shopping.errors
      end
    end
  end
end
