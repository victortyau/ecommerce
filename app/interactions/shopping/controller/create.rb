# frozen_string_literal: true

module Shopping
  module Controller
    class Create < ApplicationInteraction
      hash :shopping_params do
        string :transaction
      end

      def execute
        @shopping = Shop.new(shopping_params)
        @shopping.save ? serialize(@shopping, with: ShoppingSerializer) : @shopping.errors
      end
    end
  end
end
