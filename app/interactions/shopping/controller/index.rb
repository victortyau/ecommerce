# frozen_string_literal: true

module Shopping
  module Controller
    class Index < ApplicationInteraction
      def execute
        serialize_collection(shopping, with: ShoppingSerializer)
      end

      def shopping
        @shopping ||= Shop.last(25).reverse
      end
    end
  end
end
