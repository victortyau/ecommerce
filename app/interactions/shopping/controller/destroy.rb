# frozen_string_literal: true

module Shopping
  module Controller
    class Destroy < ApplicationInteraction
      object :shopping

      def execute
        shopping.destroy
        { message: 'shopping successfuly destroy' }
      end
    end
  end
end
