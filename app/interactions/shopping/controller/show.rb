# frozen_string_literal: true

module Shopping
  module Controller
    class Show < ApplicationInteraction
      object :shopping

      def execute
        serialize(shopping, with: ShoppingSerializer)
      end
    end
  end
end
