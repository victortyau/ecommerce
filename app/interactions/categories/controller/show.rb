# frozen_string_literal: true

module Categories
  module Controller
    class Show < ApplicationInteraction
      object :category

      def execute
        serialize(category, with: CategorySerializer)
      end
    end
  end
end
