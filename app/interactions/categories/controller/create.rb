# frozen_string_literal: true

module Categories
  module Controller
    class Create < ApplicationInteraction
      hash :category_params do
        string :name
      end

      def execute
        @category = Category.new(category_params)
        @category.save ? serialize(@category, with: CategorySerializer) : @category.errors
      end
    end
  end
end
