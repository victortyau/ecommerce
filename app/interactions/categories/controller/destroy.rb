# frozen_string_literal: true

module Categories
  module Controller
    class Destroy < ApplicationInteraction
      object :category

      def execute
        category.destroy
        { message: 'category successfuly destroy' }
      end
    end
  end
end
