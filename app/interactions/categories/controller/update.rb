# frozen_string_literal: true

module Categories
  module Controller
    class Update < ApplicationInteraction
      object :category
      hash :category_params do
        string :name
      end

      def execute
        category.update(category_params) ? serialize(category, with: CategorySerializer) : category.errors
      end
    end
  end
end
