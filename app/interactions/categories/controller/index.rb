# frozen_string_literal: true

module Categories
  module Controller
    class Index < ApplicationInteraction
      def execute
        serialize_collection(categories, with: CategorySerializer)
      end

      def categories
        @categories ||= Category.last(25).reverse
      end
    end
  end
end
