# frozen_string_literal: true

class ApplicationInteraction < ActiveInteraction::Base
  def serialize(resource, with:)
    with.new(resource).serialize
  end

  def serialize_collection(resources, with:)
    resources.find_each.map do |resource|
      serialize(resource, with: with)
    end
  end
end
