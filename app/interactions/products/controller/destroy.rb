# frozen_string_literal: true

module Products
  module Controller
    class Destroy < ApplicationInteraction
      object :product

      def execute
        product.destroy
        { message: 'product successfuly destroy' }
      end
    end
  end
end
