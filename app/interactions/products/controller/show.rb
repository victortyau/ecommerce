# frozen_string_literal: true

module Products
  module Controller
    class Show < ApplicationInteraction
      object :product

      def execute
        serialize(product, with: ProductSerializer)
      end
    end
  end
end
