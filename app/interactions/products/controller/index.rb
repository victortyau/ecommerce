# frozen_string_literal: true

module Products
  module Controller
    class Index < ApplicationInteraction
      def execute
        serialize_collection(products, with: ProductSerializer)
      end

      def products
        @products ||= Product.last(25).reverse
      end
    end
  end
end
