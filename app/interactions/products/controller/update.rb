# frozen_string_literal: true

module Products
  module Controller
    class Update < ApplicationInteraction
      object :product
      hash :product_params do
        string :name
      end

      def execute
        product.update(product_params) ? serialize(product, with: ProductSerializer) : product.errors
      end
    end
  end
end
