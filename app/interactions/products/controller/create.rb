# frozen_string_literal: true

module Products
  module Controller
    class Create < ApplicationInteraction
      hash :product_params do
        string :name
      end

      def execute
        @product = Product.new(product_params)
        @product.save ? serialize(@product, with: ProductSerializer) : @product.errors
      end
    end
  end
end
