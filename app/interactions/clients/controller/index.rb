# frozen_string_literal: true

module Clients
  module Controller
    class Index < ApplicationInteraction
      object :client

      def execute
        serialize_collection(clients, with: ClientSerializer)
      end

      def clients
        @clients ||= Client.last(25).reverse
      end
    end
  end
end
