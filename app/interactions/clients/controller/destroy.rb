# frozen_string_literal: true

module Clients
  module Controller
    class Destroy < ApplicationInteraction
      object :client

      def execute
        client.destroy
        { message: 'client successfuly destroy' }
      end
    end
  end
end
