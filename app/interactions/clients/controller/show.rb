# frozen_string_literal: true

module Clients
  module Controller
    class Show < ApplicationInteraction
      object :client

      def execute
        serialize(client, with: ClientSerializer)
      end
    end
  end
end
