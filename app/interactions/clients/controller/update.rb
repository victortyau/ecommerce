# frozen_string_literal: true

module Clients
  module Controller
    class Update < ApplicationInteraction
      object :client
      hash :client_params do
        string :name
        string :username
      end

      def execute
        client.update(client_params) ? serialize(client, with: ClientSerializer) : client.errors
      end
    end
  end
end
