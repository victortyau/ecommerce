# frozen_string_literal: true

module Clients
  module Controller
    class Create < ApplicationInteraction
      hash :client_params do
        string :name
        string :username
      end

      def execute
        @client = Client.new(client_params)
        @client.save ? serialize(@client, with: ClientSerializer) : @client.errors
      end
    end
  end
end
