# frozen_string_literal: true

module Api
  module V1
    class ApiController < ApplicationController
      include ::Authentication
      include ::Authorization

      rescue_from ActionController::ParameterMissing, with: :handle_record_invalid
      rescue_from ActiveRecord::RecordInvalid, with: :handle_record_invalid
      rescue_from ActiveRecord::RecordNotFound, with: :handle_not_found

      private

      def handle_record_invalid(exception)
        render_error(exception.message, :unprocessable_entity)
      end

      def handle_not_found(exception)
        render_error(exception.message, :not_found)
      end

      def render_error(errors, status)
        render json: { errors: Array(errors) }, status: status
      end
    end
  end
end
