# frozen_string_literal: true

class ApplicationController < ActionController::API
  def render_outcome(outcome, success_status: 200, error_status: 422)
    if outcome.valid?
      render_success(outcome.result, status: success_status)
    else
      render_error(outcome.errors, status: error_status)
    end
  end

  def render_success(result, status:)
    render status: status, json: { data: result }
  end

  def render_error(errors, status:)
    render status: status, json: { data: errors.to_a }
  end
end

