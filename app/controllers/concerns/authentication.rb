# frozen_string_literal: true

module Authentication
  extend ActiveSupport::Concern

  private

  def authenticate
    return if current_user?

    render_error('Login errors', :unauthorized)
  end

  def current_user?
    !!current_user
  end

  def current_user
    return if auth_token.blank?

    @current_user = User.find(auth_token[:user_id])
  end

  def auth_token
    return if request.headers['Authorization'].blank?

    token = request.headers['Authorization'].split.last
    JsonWebToken.decode(token)
  rescue JWT::DecodeError
    nil
  end
end
